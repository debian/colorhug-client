#include <gio/gio.h>

#if defined (__ELF__) && ( __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 6))
# define SECTION __attribute__ ((section (".gresource.ch"), aligned (8)))
#else
# define SECTION
#endif

static const SECTION union { const guint8 data[4799]; const double alignment; void * const ptr;}  ch_resource_data = { {
  0x47, 0x56, 0x61, 0x72, 0x69, 0x61, 0x6e, 0x74, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x18, 0x00, 0x00, 0x00, 0x1c, 0x01, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x28, 0x09, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 
  0x04, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 
  0x06, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00, 
  0x07, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 
  0x09, 0x00, 0x00, 0x00, 0x63, 0x5d, 0x3c, 0x51, 
  0x03, 0x00, 0x00, 0x00, 0x1c, 0x01, 0x00, 0x00, 
  0x04, 0x00, 0x4c, 0x00, 0x20, 0x01, 0x00, 0x00, 
  0x24, 0x01, 0x00, 0x00, 0xc2, 0xaf, 0x89, 0x0b, 
  0x06, 0x00, 0x00, 0x00, 0x24, 0x01, 0x00, 0x00, 
  0x04, 0x00, 0x4c, 0x00, 0x28, 0x01, 0x00, 0x00, 
  0x2c, 0x01, 0x00, 0x00, 0x3b, 0xbb, 0x9c, 0x1c, 
  0x03, 0x00, 0x00, 0x00, 0x2c, 0x01, 0x00, 0x00, 
  0x0f, 0x00, 0x76, 0x00, 0x40, 0x01, 0x00, 0x00, 
  0x4f, 0x09, 0x00, 0x00, 0xee, 0x9f, 0x77, 0x1f, 
  0x04, 0x00, 0x00, 0x00, 0x4f, 0x09, 0x00, 0x00, 
  0x0a, 0x00, 0x4c, 0x00, 0x5c, 0x09, 0x00, 0x00, 
  0x68, 0x09, 0x00, 0x00, 0x76, 0xeb, 0x99, 0x3d, 
  0x07, 0x00, 0x00, 0x00, 0x68, 0x09, 0x00, 0x00, 
  0x09, 0x00, 0x4c, 0x00, 0x74, 0x09, 0x00, 0x00, 
  0x78, 0x09, 0x00, 0x00, 0x37, 0x37, 0x47, 0x0e, 
  0x00, 0x00, 0x00, 0x00, 0x78, 0x09, 0x00, 0x00, 
  0x08, 0x00, 0x76, 0x00, 0x80, 0x09, 0x00, 0x00, 
  0x8f, 0x0a, 0x00, 0x00, 0xd4, 0xb5, 0x02, 0x00, 
  0xff, 0xff, 0xff, 0xff, 0x8f, 0x0a, 0x00, 0x00, 
  0x01, 0x00, 0x4c, 0x00, 0x90, 0x0a, 0x00, 0x00, 
  0x94, 0x0a, 0x00, 0x00, 0x04, 0xc3, 0x10, 0xfd, 
  0x01, 0x00, 0x00, 0x00, 0x94, 0x0a, 0x00, 0x00, 
  0x08, 0x00, 0x4c, 0x00, 0x9c, 0x0a, 0x00, 0x00, 
  0xa0, 0x0a, 0x00, 0x00, 0xe3, 0x22, 0xa5, 0xe2, 
  0x03, 0x00, 0x00, 0x00, 0xa0, 0x0a, 0x00, 0x00, 
  0x0b, 0x00, 0x76, 0x00, 0xb0, 0x0a, 0x00, 0x00, 
  0xbf, 0x12, 0x00, 0x00, 0x67, 0x74, 0x6b, 0x2f, 
  0x05, 0x00, 0x00, 0x00, 0x63, 0x6f, 0x6d, 0x2f, 
  0x07, 0x00, 0x00, 0x00, 0x63, 0x68, 0x2d, 0x62, 
  0x61, 0x63, 0x6b, 0x6c, 0x69, 0x67, 0x68, 0x74, 
  0x2e, 0x75, 0x69, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x74, 0x4a, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 
  0x78, 0xda, 0xed, 0x5c, 0x4b, 0x73, 0xdb, 0x36, 
  0x10, 0xbe, 0xe7, 0x57, 0xa0, 0x9c, 0xe9, 0xf4, 
  0xd0, 0x91, 0x25, 0xca, 0x76, 0x9a, 0x83, 0xac, 
  0x4c, 0xd2, 0x4e, 0x9c, 0xce, 0xf8, 0xd0, 0xa9, 
  0x93, 0xc9, 0x91, 0x03, 0x92, 0x2b, 0x0a, 0x09, 
  0x48, 0xb0, 0x00, 0xa8, 0x47, 0x7e, 0x7d, 0x17, 
  0x84, 0x24, 0x5b, 0x16, 0x29, 0x3e, 0x2c, 0xdb, 
  0x92, 0x25, 0x1f, 0x3c, 0xa4, 0xc9, 0x05, 0x16, 
  0x8b, 0xdd, 0xfd, 0x3e, 0x80, 0x0b, 0x0f, 0xde, 
  0xcf, 0x62, 0x4e, 0x26, 0x20, 0x15, 0x13, 0xc9, 
  0x95, 0xe3, 0x9e, 0xf5, 0x1c, 0x02, 0x49, 0x20, 
  0x42, 0x96, 0x44, 0x57, 0xce, 0xd7, 0x2f, 0x9f, 
  0x3a, 0xef, 0x9c, 0xf7, 0xc3, 0x37, 0x83, 0x5f, 
  0x3a, 0x1d, 0x72, 0x0d, 0x09, 0x48, 0xaa, 0x21, 
  0x24, 0x53, 0xa6, 0xc7, 0x24, 0xe2, 0x34, 0x04, 
  0x72, 0x7e, 0xe6, 0xbe, 0x3b, 0x73, 0x49, 0xa7, 
  0x83, 0x2f, 0xb1, 0x44, 0x83, 0x1c, 0xd1, 0x00, 
  0x86, 0x6f, 0x08, 0x19, 0x48, 0xf8, 0x2f, 0x63, 
  0x12, 0x14, 0xe1, 0xcc, 0xbf, 0x72, 0x22, 0xfd, 
  0xe3, 0x77, 0xe7, 0xae, 0x23, 0x14, 0xeb, 0x3b, 
  0xdd, 0xfc, 0x3d, 0xe1, 0x7f, 0x87, 0x40, 0x93, 
  0x80, 0x53, 0xa5, 0xae, 0x9c, 0x6b, 0xfd, 0xe3, 
  0x43, 0xf8, 0x3d, 0x53, 0x3a, 0x86, 0x44, 0x3b, 
  0x84, 0x85, 0x57, 0x0e, 0x5d, 0xdd, 0x7b, 0x11, 
  0x8d, 0x63, 0xea, 0x18, 0x31, 0x14, 0x4c, 0xa5, 
  0x48, 0x41, 0xea, 0x39, 0x49, 0x68, 0x0c, 0x57, 
  0x0e, 0x17, 0x53, 0x90, 0xce, 0xb0, 0x77, 0xe6, 
  0xf6, 0xd6, 0x7f, 0xdc, 0x41, 0x77, 0xf9, 0x6a, 
  0xb1, 0x64, 0x96, 0xa6, 0x46, 0xb2, 0x5f, 0xf5, 
  0x9e, 0xd2, 0x90, 0x7a, 0x2c, 0x09, 0x24, 0xe4, 
  0xca, 0x61, 0x57, 0xbd, 0xcb, 0x07, 0x7d, 0x9d, 
  0x57, 0xb5, 0x91, 0xd2, 0x08, 0x9a, 0xb5, 0x31, 
  0xe8, 0x5a, 0x0b, 0x35, 0x36, 0x96, 0x84, 0x11, 
  0x9a, 0x7f, 0xfc, 0x54, 0xe6, 0xaa, 0x7c, 0x6f, 
  0x42, 0x79, 0x06, 0xa6, 0x87, 0xcb, 0xe6, 0x86, 
  0x6d, 0xac, 0xd4, 0xa6, 0x5d, 0x2b, 0x9a, 0x68, 
  0x6d, 0x56, 0x15, 0x0b, 0xa1, 0x2b, 0xad, 0xba, 
  0x3b, 0x33, 0x16, 0x78, 0xdd, 0xb3, 0x5b, 0x23, 
  0x4d, 0x39, 0x0b, 0xa8, 0xc6, 0xd0, 0xfd, 0xc6, 
  0x92, 0x50, 0x4c, 0xad, 0x51, 0x42, 0x46, 0xb9, 
  0x88, 0x3c, 0x9f, 0x06, 0x3f, 0x38, 0x8b, 0xc6, 
  0xba, 0xc4, 0x26, 0x01, 0x4d, 0xbc, 0x91, 0x08, 
  0x32, 0xe5, 0x0c, 0x3f, 0x51, 0xae, 0x60, 0x43, 
  0xf9, 0x60, 0xcc, 0x78, 0x68, 0xaf, 0x8b, 0x7a, 
  0xbf, 0xd5, 0xd8, 0x81, 0xed, 0x51, 0x99, 0xcb, 
  0x8d, 0x0e, 0x0b, 0x9d, 0x8f, 0x29, 0xe6, 0x73, 
  0x74, 0xbf, 0x2f, 0x32, 0xdb, 0xe8, 0xb1, 0x8d, 
  0x96, 0x45, 0x32, 0x5a, 0xd2, 0x44, 0x31, 0x63, 
  0x16, 0x2f, 0xcc, 0x64, 0x6e, 0x1f, 0x67, 0x78, 
  0xd1, 0xeb, 0x35, 0x94, 0xd5, 0xf3, 0x14, 0x15, 
  0x0d, 0xa4, 0x50, 0x6a, 0x84, 0x39, 0xb5, 0x50, 
  0x7a, 0xcd, 0x46, 0xc5, 0x76, 0xfa, 0x28, 0x66, 
  0xd6, 0x4a, 0xbe, 0x98, 0x79, 0x81, 0x48, 0x12, 
  0x7c, 0xec, 0xdc, 0x17, 0x69, 0x61, 0xa5, 0xb6, 
  0x96, 0x2a, 0x92, 0x1b, 0x53, 0x9c, 0x34, 0x34, 
  0x50, 0x00, 0x06, 0x27, 0xea, 0x4a, 0x4d, 0x5a, 
  0x49, 0xf9, 0x42, 0x86, 0x20, 0xbd, 0x29, 0x0b, 
  0x4d, 0xa8, 0x9e, 0xf7, 0xea, 0xca, 0xa9, 0x94, 
  0x06, 0x08, 0x7d, 0x08, 0x06, 0x17, 0xa5, 0x22, 
  0x1b, 0x53, 0x51, 0x3c, 0x1d, 0x7f, 0xc7, 0x18, 
  0x84, 0x76, 0x42, 0x98, 0xb9, 0xf4, 0x32, 0xe5, 
  0x3b, 0x0f, 0xc5, 0x36, 0x35, 0xc8, 0x55, 0xf6, 
  0x0c, 0x78, 0x82, 0xc2, 0xe9, 0x73, 0xff, 0xb8, 
  0x2c, 0x53, 0xa4, 0xd0, 0xc6, 0x60, 0xe2, 0xa2, 
  0xb5, 0x78, 0x2d, 0x7f, 0x78, 0x8c, 0x4f, 0x14, 
  0xc9, 0xce, 0x53, 0x1a, 0xa2, 0xa6, 0xfd, 0x26, 
  0x32, 0x4a, 0x0b, 0xcc, 0x09, 0x43, 0x64, 0x15, 
  0x1d, 0xbc, 0x4c, 0x9b, 0x88, 0x32, 0x0c, 0x0d, 
  0x4f, 0xb1, 0x9f, 0x38, 0xca, 0xb7, 0xe5, 0x72, 
  0xf7, 0x13, 0xe2, 0x7a, 0x6b, 0x98, 0x7c, 0xd0, 
  0x43, 0xaa, 0xfb, 0x81, 0x59, 0x4a, 0x93, 0xb0, 
  0x85, 0x3d, 0x46, 0x8c, 0xf3, 0x16, 0x62, 0xa9, 
  0xb0, 0xe9, 0x04, 0xb3, 0xfd, 0xb6, 0x61, 0x15, 
  0xea, 0x3f, 0xe8, 0x16, 0x38, 0x75, 0x5d, 0x47, 
  0xbf, 0xa1, 0x3e, 0x70, 0xeb, 0xe8, 0xdc, 0x5c, 
  0x22, 0xf0, 0x68, 0x29, 0x9c, 0x3d, 0xf5, 0xb6, 
  0x5c, 0x45, 0x67, 0xf8, 0x0f, 0x07, 0xaa, 0x80, 
  0xb0, 0x44, 0xe1, 0x33, 0x32, 0x17, 0x99, 0x24, 
  0x7f, 0x0a, 0x2e, 0xe4, 0xe7, 0x2c, 0xfa, 0x70, 
  0x73, 0x4b, 0x42, 0x98, 0xb0, 0x00, 0xce, 0x9a, 
  0x34, 0x3c, 0x95, 0x34, 0x6d, 0x3e, 0x14, 0x23, 
  0xe5, 0xc5, 0x22, 0x44, 0x2b, 0x4c, 0x31, 0x67, 
  0x75, 0x82, 0x31, 0x95, 0x4d, 0xe4, 0x63, 0x3a, 
  0xb3, 0x59, 0xce, 0x33, 0x92, 0xca, 0xc0, 0x4f, 
  0x13, 0xf1, 0xd9, 0x22, 0xbb, 0x6e, 0x17, 0xa2, 
  0x5a, 0x4b, 0xe6, 0x67, 0x1a, 0xd4, 0xe6, 0xc3, 
  0xfb, 0x8f, 0x97, 0xc1, 0x19, 0x50, 0x9c, 0x55, 
  0x92, 0xf3, 0x3f, 0xb3, 0xa6, 0x58, 0x30, 0xfd, 
  0x87, 0x9e, 0x58, 0xde, 0xea, 0x01, 0x06, 0x9f, 
  0xe1, 0x25, 0x16, 0xc7, 0x21, 0x09, 0xdb, 0x85, 
  0xad, 0xbb, 0x8b, 0xb0, 0x2d, 0x32, 0x5d, 0xb1, 
  0xd9, 0x1e, 0x2a, 0x62, 0x7e, 0x23, 0xcc, 0x5a, 
  0xde, 0x50, 0x17, 0x2f, 0x35, 0xd3, 0x26, 0x80, 
  0x0d, 0xdf, 0x2c, 0x71, 0xa1, 0x02, 0xd5, 0x37, 
  0xd4, 0x6e, 0xcc, 0x6e, 0x70, 0x5d, 0x93, 0x71, 
  0xad, 0xf6, 0x89, 0xdd, 0xac, 0x33, 0x8e, 0xda, 
  0x84, 0x43, 0x48, 0x86, 0xc4, 0x66, 0x41, 0x1d, 
  0x71, 0x79, 0xac, 0x91, 0x67, 0xf3, 0xc7, 0x52, 
  0x8f, 0x6b, 0xc9, 0x42, 0x6b, 0xac, 0x08, 0xaf, 
  0x4a, 0xac, 0xb5, 0x37, 0x19, 0xb9, 0x1e, 0x2f, 
  0xdc, 0xc6, 0x0d, 0x71, 0x59, 0x20, 0x75, 0xb3, 
  0xac, 0x29, 0x23, 0x96, 0x78, 0x1c, 0x46, 0x86, 
  0x23, 0xbd, 0x6b, 0x21, 0x2a, 0xed, 0xfa, 0xa3, 
  0x95, 0x2c, 0x12, 0x96, 0x96, 0x92, 0xbe, 0xd0, 
  0x5a, 0xc4, 0x4d, 0x85, 0xa5, 0x98, 0x7a, 0x2b, 
  0x62, 0xfb, 0xb6, 0xd1, 0xb4, 0x0a, 0x9e, 0xc5, 
  0xc9, 0x9d, 0x70, 0x05, 0x49, 0x2b, 0x74, 0xcf, 
  0x9a, 0xa4, 0x81, 0x67, 0x33, 0x6f, 0x91, 0x4d, 
  0x0a, 0x1a, 0x78, 0x84, 0xab, 0x3e, 0xd6, 0x5d, 
  0xab, 0xfd, 0xa7, 0xdf, 0x54, 0xdc, 0x72, 0x10, 
  0x92, 0x2f, 0x01, 0x39, 0xd5, 0x14, 0xc7, 0x81, 
  0x34, 0x18, 0x94, 0x43, 0x02, 0x11, 0x9b, 0x15, 
  0x3b, 0xda, 0xe8, 0xcb, 0x18, 0x88, 0xca, 0xa4, 
  0x14, 0x59, 0x62, 0x36, 0xe3, 0x48, 0xbe, 0xe0, 
  0x25, 0x1c, 0x26, 0x86, 0xbd, 0x7c, 0x88, 0x7d, 
  0x93, 0x36, 0x9a, 0xf6, 0xbb, 0x44, 0x7b, 0xb7, 
  0x52, 0x50, 0xe9, 0x39, 0x87, 0xe2, 0x67, 0x66, 
  0x9e, 0xcd, 0x34, 0x2e, 0xda, 0x0c, 0x59, 0xdc, 
  0xb1, 0xe3, 0xe9, 0x96, 0xb4, 0xd5, 0x2d, 0x6d, 
  0xac, 0x0c, 0xe7, 0xb7, 0x63, 0x7d, 0xa1, 0x45, 
  0x71, 0x26, 0x3c, 0x24, 0x14, 0x34, 0x18, 0x57, 
  0x90, 0x99, 0x42, 0x14, 0x13, 0x69, 0x5d, 0xe1, 
  0x12, 0x24, 0x2e, 0x25, 0xd1, 0xbb, 0x88, 0x8b, 
  0x23, 0x88, 0x88, 0xf5, 0xac, 0x7a, 0xd9, 0x6b, 
  0x17, 0x51, 0xc3, 0x1e, 0xb9, 0xc9, 0x66, 0x6d, 
  0xa3, 0xa2, 0x72, 0xe6, 0x9f, 0xc4, 0x59, 0xdd, 
  0x27, 0x74, 0xd6, 0x92, 0xdd, 0x85, 0x82, 0xad, 
  0xee, 0x17, 0x76, 0x72, 0x19, 0xf9, 0xa7, 0xe4, 
  0xff, 0x20, 0xf9, 0xaf, 0x76, 0x39, 0xf1, 0xef, 
  0xb8, 0x30, 0x75, 0x86, 0xf9, 0xfa, 0xf4, 0x94, 
  0xf2, 0x77, 0x9f, 0xf2, 0xdd, 0xfd, 0x8b, 0x86, 
  0xa3, 0x89, 0x83, 0x61, 0x2f, 0x56, 0x47, 0x94, 
  0xb0, 0xdd, 0x57, 0x92, 0xb0, 0xed, 0x47, 0xb0, 
  0xc3, 0xc9, 0xd9, 0x75, 0x93, 0x2e, 0x8d, 0x91, 
  0x6f, 0x6b, 0x22, 0x46, 0xc4, 0x8e, 0xd0, 0x30, 
  0xef, 0x50, 0x24, 0x40, 0xb4, 0x20, 0x7a, 0x2d, 
  0x29, 0x4b, 0xa0, 0x86, 0x97, 0xa3, 0x42, 0xb7, 
  0xcb, 0x37, 0x4f, 0xb9, 0x79, 0xf7, 0xb9, 0xf9, 
  0x85, 0x1d, 0xff, 0xd6, 0x6e, 0x65, 0xe6, 0xdf, 
  0x1e, 0xcd, 0xe5, 0xfa, 0xd7, 0xdf, 0x86, 0xdf, 
  0x72, 0xfa, 0xbd, 0xc6, 0xa6, 0xd8, 0x55, 0xd0, 
  0xb4, 0x11, 0xbf, 0xfb, 0xe6, 0xed, 0x0c, 0x37, 
  0xbe, 0x7f, 0x37, 0x6d, 0x4c, 0xa2, 0x05, 0x24, 
  0x0b, 0xb4, 0xa7, 0x85, 0x67, 0xf6, 0x58, 0xbd, 
  0xc5, 0x4a, 0xb6, 0x55, 0x34, 0x87, 0x92, 0x4e, 
  0xbd, 0x45, 0x79, 0x41, 0xab, 0x06, 0x72, 0x59, 
  0x2f, 0x15, 0x68, 0x99, 0x7c, 0xc9, 0x71, 0x78, 
  0x40, 0xb2, 0x77, 0x0c, 0xde, 0x16, 0x9b, 0xbc, 
  0x3e, 0x44, 0x30, 0x15, 0x4f, 0x98, 0xfa, 0xa9, 
  0x26, 0x53, 0xf8, 0x4d, 0x02, 0x51, 0x34, 0x4e, 
  0xb9, 0x81, 0x05, 0x03, 0x07, 0x0a, 0x12, 0x65, 
  0xa8, 0xf9, 0xbf, 0x76, 0xf4, 0x27, 0x00, 0xd8, 
  0x3d, 0x00, 0x5c, 0xec, 0x19, 0x00, 0xac, 0x57, 
  0x55, 0x1d, 0x25, 0x02, 0xc8, 0x76, 0xde, 0x7e, 
  0x82, 0x80, 0xdd, 0x42, 0xc0, 0xc5, 0x7e, 0xae, 
  0x09, 0x16, 0x93, 0x72, 0x28, 0x08, 0x60, 0x6a, 
  0xe3, 0x0e, 0x6b, 0x01, 0xda, 0x7f, 0x8d, 0xbc, 
  0xe1, 0xd0, 0xbc, 0xc6, 0xed, 0x1d, 0xdc, 0xc6, 
  0x45, 0xff, 0x35, 0x25, 0x9b, 0xbc, 0x12, 0xfc, 
  0xd5, 0xee, 0x3f, 0xdc, 0x71, 0x4b, 0x82, 0x3d, 
  0x13, 0x96, 0x8c, 0x30, 0x3a, 0x92, 0x00, 0xec, 
  0x1e, 0x44, 0x8e, 0x57, 0x09, 0x28, 0x54, 0xe6, 
  0x1b, 0x54, 0x63, 0xd7, 0x89, 0x77, 0xb6, 0x71, 
  0xf8, 0xf3, 0x3d, 0xe3, 0x9d, 0xf7, 0x8f, 0x3e, 
  0x1c, 0x25, 0xeb, 0xcc, 0x0d, 0x70, 0xe2, 0x9c, 
  0x2f, 0xcb, 0x39, 0xcf, 0xf7, 0x12, 0x06, 0x0e, 
  0x90, 0x72, 0x5e, 0x1c, 0x11, 0x77, 0xd8, 0xb9, 
  0xd3, 0x1c, 0x67, 0x7d, 0x68, 0x7f, 0x47, 0x65, 
  0xdd, 0x85, 0x30, 0xbc, 0x86, 0xe5, 0x13, 0x06, 
  0xd3, 0x87, 0x30, 0x5e, 0x00, 0xdf, 0x8f, 0xad, 
  0x35, 0x5d, 0xd4, 0x25, 0xb6, 0xa8, 0x35, 0x75, 
  0xeb, 0xca, 0x54, 0x56, 0xd7, 0x56, 0x97, 0xa7, 
  0xae, 0x8f, 0x72, 0xed, 0xa1, 0x4d, 0x5d, 0xc4, 
  0xcc, 0xeb, 0x42, 0x3d, 0x9f, 0x4a, 0xa7, 0xfc, 
  0x24, 0xd3, 0x67, 0xa0, 0x21, 0xc8, 0x8f, 0xf8, 
  0x4e, 0x9e, 0xc2, 0xc6, 0xf9, 0xed, 0x8b, 0x1f, 
  0x62, 0xb2, 0x66, 0x5d, 0x56, 0xbf, 0x93, 0x8f, 
  0xab, 0x4f, 0x5c, 0x5f, 0x35, 0xe3, 0x4c, 0xcf, 
  0xeb, 0x34, 0xa2, 0x32, 0x7f, 0xd1, 0xce, 0x57, 
  0x65, 0x36, 0x47, 0x37, 0x6b, 0xe9, 0x6b, 0xb5, 
  0x32, 0x16, 0x53, 0x2f, 0xe0, 0x42, 0x81, 0xe7, 
  0x67, 0x5a, 0x9b, 0x69, 0x2b, 0x1d, 0x79, 0x9d, 
  0x92, 0x61, 0xdb, 0x86, 0xad, 0x1a, 0xce, 0xaf, 
  0xbd, 0x2c, 0x7d, 0xe2, 0x9a, 0xe1, 0x26, 0x62, 
  0x12, 0x02, 0x60, 0x13, 0x50, 0x5e, 0x08, 0x23, 
  0x8a, 0x91, 0xd0, 0x4c, 0x9a, 0xf2, 0x29, 0x9d, 
  0x2b, 0x2f, 0xb7, 0x59, 0x7e, 0xc0, 0xa8, 0x42, 
  0xbc, 0x66, 0xed, 0xf0, 0x5a, 0x9d, 0x75, 0x99, 
  0xd5, 0xf6, 0xa6, 0x76, 0x78, 0x55, 0x99, 0x7a, 
  0xbe, 0xab, 0xc2, 0xd4, 0x8d, 0x63, 0x5b, 0xdb, 
  0x6c, 0xb0, 0x77, 0x04, 0x23, 0x3f, 0xdc, 0x64, 
  0xb3, 0x6b, 0x24, 0x3a, 0x59, 0xda, 0x51, 0xf3, 
  0xd8, 0x17, 0x9c, 0x05, 0xcf, 0xc5, 0x1a, 0x6a, 
  0x03, 0x6b, 0x39, 0xb8, 0xb6, 0xb1, 0x5b, 0xad, 
  0xc3, 0x4f, 0x2f, 0xc2, 0x50, 0x0f, 0xca, 0x7d, 
  0xea, 0x6c, 0x53, 0xdc, 0xe1, 0x43, 0x96, 0x92, 
  0xcb, 0x5f, 0x31, 0xe3, 0xa7, 0xcf, 0xed, 0x5c, 
  0x6d, 0x4c, 0xb3, 0x0b, 0xdf, 0x72, 0x9f, 0x87, 
  0xc8, 0xb6, 0xa5, 0x6d, 0x2a, 0x8b, 0x22, 0x5c, 
  0xef, 0x42, 0xd8, 0xa1, 0x41, 0xae, 0x70, 0x1b, 
  0x0a, 0xd7, 0xee, 0x70, 0xce, 0x26, 0xd2, 0x86, 
  0x62, 0x9a, 0x9c, 0xb0, 0xb6, 0x39, 0xd6, 0x16, 
  0xd8, 0xed, 0x08, 0xd1, 0xb6, 0xc4, 0x0a, 0x7b, 
  0x8e, 0xb7, 0x46, 0xeb, 0x13, 0xe2, 0xee, 0x09, 
  0xe2, 0x1e, 0x8a, 0x0b, 0x35, 0xc3, 0x5c, 0x33, 
  0xaa, 0x1c, 0x75, 0xff, 0xc2, 0x8b, 0x13, 0xee, 
  0xbe, 0x5a, 0xdc, 0x2d, 0xdd, 0x75, 0xd8, 0xfe, 
  0x9f, 0x5a, 0x6e, 0xd9, 0x4f, 0xb8, 0x96, 0x02, 
  0xd9, 0xa6, 0xfd, 0x74, 0x80, 0xb7, 0x91, 0xb9, 
  0xf5, 0xec, 0x5e, 0xc3, 0x22, 0x36, 0x96, 0x47, 
  0x41, 0x07, 0x53, 0x16, 0x46, 0xa0, 0x57, 0xe7, 
  0xaf, 0x17, 0xf7, 0xcb, 0x13, 0xad, 0x2b, 0xe6, 
  0xda, 0xdd, 0xfa, 0x42, 0x1e, 0x68, 0xdd, 0xa5, 
  0xa2, 0xf7, 0x9a, 0xbc, 0x53, 0x75, 0xd0, 0xbd, 
  0xf7, 0xbf, 0xa1, 0xfe, 0x07, 0xcf, 0xb1, 0xc1, 
  0xc8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x28, 0x75, 0x75, 0x61, 0x79, 0x29, 0x42, 
  0x61, 0x63, 0x6b, 0x6c, 0x69, 0x67, 0x68, 0x74, 
  0x2f, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 
  0x43, 0x6f, 0x6c, 0x6f, 0x72, 0x48, 0x75, 0x67, 
  0x2f, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 
  0x6d, 0x65, 0x6e, 0x75, 0x73, 0x2e, 0x75, 0x69, 
  0x02, 0x02, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 
  0x78, 0xda, 0xa5, 0x91, 0xc1, 0x6e, 0xc2, 0x30, 
  0x10, 0x44, 0xef, 0x7c, 0xc5, 0xe2, 0x6b, 0xe5, 
  0x98, 0xaa, 0xd7, 0x8d, 0x51, 0x14, 0x7e, 0x00, 
  0xa9, 0x37, 0x84, 0xaa, 0x4d, 0xba, 0xad, 0x2c, 
  0x1c, 0x03, 0xf6, 0xba, 0x6a, 0xff, 0xbe, 0x0e, 
  0x15, 0x28, 0x17, 0x5a, 0x15, 0x8e, 0x6f, 0xec, 
  0xd9, 0x1d, 0x7b, 0x70, 0xf9, 0x39, 0x78, 0xf8, 
  0xe0, 0x98, 0xdc, 0x3e, 0xd4, 0xea, 0xb1, 0x5a, 
  0xa8, 0xa5, 0x9d, 0xa1, 0x0b, 0xc2, 0xf1, 0x8d, 
  0x7a, 0xb6, 0x33, 0x00, 0x9c, 0x6b, 0x0d, 0x17, 
  0x45, 0x47, 0x3e, 0x66, 0x17, 0x39, 0xc1, 0xbb, 
  0xec, 0x1e, 0xe0, 0xa9, 0x5a, 0x80, 0xd6, 0xa7, 
  0x6b, 0x03, 0x87, 0x0c, 0xee, 0xb5, 0x56, 0x74, 
  0x38, 0xe8, 0x11, 0xd4, 0x28, 0x97, 0x83, 0xc4, 
  0xbd, 0x94, 0xf1, 0x3f, 0x54, 0xd8, 0x09, 0x0f, 
  0x67, 0x28, 0x48, 0x22, 0xd1, 0x75, 0x59, 0x18, 
  0x02, 0x0d, 0x5c, 0x2b, 0x4f, 0x1d, 0x7b, 0x05, 
  0x12, 0x29, 0x24, 0x4f, 0x42, 0x9d, 0x2f, 0xe2, 
  0x17, 0x27, 0x65, 0x5f, 0x9a, 0x6e, 0x9f, 0x05, 
  0xcd, 0xc5, 0xf1, 0xcb, 0x14, 0x3a, 0xed, 0x54, 
  0xb6, 0x84, 0xa9, 0xe8, 0x8a, 0x0d, 0xcd, 0x34, 
  0xca, 0x1d, 0xb9, 0xd6, 0xd9, 0xdd, 0x10, 0xeb, 
  0xf8, 0x0f, 0x57, 0x5f, 0x56, 0x5b, 0x9c, 0x6f, 
  0xda, 0x55, 0xf3, 0xdc, 0x6c, 0xb0, 0x95, 0xe8, 
  0xed, 0x7a, 0xbb, 0xb5, 0x7f, 0xbc, 0x0a, 0xcd, 
  0xe4, 0xf3, 0xd1, 0x8c, 0xad, 0x94, 0x7a, 0xcd, 
  0xa4, 0xdf, 0x6f, 0x1d, 0xb2, 0x9e, 0xae, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x28, 0x75, 0x75, 0x61, 0x79, 0x29, 0x2f, 
  0x01, 0x00, 0x00, 0x00, 0x68, 0x75, 0x67, 0x68, 
  0x73, 0x6b, 0x69, 0x2f, 0x04, 0x00, 0x00, 0x00, 
  0x75, 0x73, 0x62, 0x2d, 0x61, 0x6c, 0x73, 0x2e, 
  0x73, 0x76, 0x67, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x66, 0x14, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 
  0x78, 0xda, 0xe5, 0x57, 0x4b, 0x6f, 0xe3, 0x36, 
  0x10, 0xbe, 0xe7, 0x57, 0xa8, 0xca, 0x25, 0x41, 
  0x43, 0x89, 0xd4, 0x5b, 0x8a, 0xed, 0x3d, 0x74, 
  0xb1, 0xc0, 0x02, 0x3d, 0xb5, 0x5b, 0xf4, 0x4c, 
  0x4b, 0xb4, 0xad, 0x46, 0x12, 0x0d, 0x8a, 0x8e, 
  0xe3, 0xfd, 0xf5, 0x1d, 0x52, 0x6f, 0xd9, 0x4e, 
  0xd2, 0xc3, 0xf6, 0xb1, 0x95, 0x61, 0xd8, 0x9c, 
  0x07, 0xc9, 0xf9, 0xe6, 0x9b, 0x21, 0xb5, 0xf8, 
  0xf0, 0x52, 0x16, 0xc6, 0x33, 0x13, 0x75, 0xce, 
  0xab, 0xa5, 0x49, 0x2c, 0x6c, 0x1a, 0xac, 0x4a, 
  0x79, 0x96, 0x57, 0xdb, 0xa5, 0xf9, 0xdb, 0x97, 
  0x4f, 0x28, 0x32, 0x8d, 0x5a, 0xd2, 0x2a, 0xa3, 
  0x05, 0xaf, 0xd8, 0xd2, 0xac, 0xb8, 0xf9, 0x61, 
  0x75, 0xb3, 0xf8, 0x01, 0x21, 0xe3, 0x27, 0xc1, 
  0xa8, 0x64, 0x99, 0x71, 0xcc, 0xe5, 0xce, 0xf8, 
  0x5c, 0x3d, 0xd5, 0x29, 0xdd, 0x33, 0xe3, 0x6e, 
  0x27, 0xe5, 0x3e, 0xb1, 0xed, 0xe3, 0xf1, 0x68, 
  0xe5, 0xad, 0xd0, 0xe2, 0x62, 0x6b, 0xdf, 0x1b, 
  0x08, 0xad, 0x6e, 0x6e, 0x16, 0xf5, 0xf3, 0xf6, 
  0xc6, 0x30, 0x0c, 0x58, 0xb7, 0xaa, 0x93, 0x2c, 
  0x5d, 0x9a, 0xad, 0xc3, 0xfe, 0x20, 0x0a, 0x6d, 
  0x98, 0xa5, 0x36, 0x2b, 0x58, 0xc9, 0x2a, 0x59, 
  0xdb, 0xc4, 0x22, 0xb6, 0x39, 0x98, 0xa7, 0x83, 
  0x79, 0xaa, 0x56, 0xcf, 0x9f, 0x59, 0xca, 0xcb, 
  0x92, 0x57, 0xb5, 0xf6, 0xac, 0xea, 0xdb, 0x91, 
  0xb1, 0xc8, 0x36, 0xbd, 0xb5, 0xda, 0xcd, 0xd1, 
  0xd5, 0x46, 0x24, 0x8e, 0x63, 0x1b, 0x3b, 0xb6, 
  0xe3, 0x20, 0xb0, 0x40, 0xf5, 0xa9, 0x92, 0xf4, 
  0x05, 0x4d, 0x5d, 0x61, 0x8f, 0x97, 0x5c, 0x1d, 
  0x8c, 0xb1, 0x0d, 0xba, 0xc1, 0xf2, 0x7d, 0x56, 
  0x49, 0x0d, 0x80, 0xee, 0xe1, 0xdb, 0x9b, 0x77, 
  0x02, 0xab, 0xe6, 0x07, 0x91, 0xb2, 0x0d, 0xf8, 
  0x31, 0xab, 0x62, 0xd2, 0xfe, 0xf8, 0xe5, 0x63, 
  0xaf, 0x44, 0xd8, 0xca, 0x64, 0x36, 0x9a, 0xa6, 
  0xc3, 0x73, 0xb2, 0xea, 0x04, 0xe4, 0x8a, 0x96, 
  0xac, 0xde, 0xd3, 0x94, 0xd5, 0x76, 0x27, 0xd7, 
  0xfe, 0xc7, 0x3c, 0x93, 0x3b, 0xc8, 0x2f, 0x76, 
  0x2d, 0x1c, 0x6b, 0xc9, 0x8e, 0xe5, 0xdb, 0x9d, 
  0x5c, 0x9a, 0x5e, 0xa0, 0x87, 0x79, 0xb6, 0x34, 
  0x61, 0xcf, 0x2e, 0x09, 0x1c, 0x3d, 0x1e, 0x51, 
  0x82, 0x34, 0x06, 0xed, 0x74, 0x49, 0xaf, 0xc1, 
  0x56, 0x4c, 0x0c, 0x41, 0xdc, 0xd0, 0xf1, 0xb5, 
  0x45, 0xb7, 0xed, 0x24, 0xe3, 0xa9, 0xda, 0xc7, 
  0xd2, 0x3c, 0xd4, 0x6b, 0x44, 0x8b, 0xda, 0x52, 
  0x60, 0xac, 0xc0, 0x64, 0x91, 0xb1, 0x4d, 0xad, 
  0x4c, 0x9b, 0xf5, 0xd4, 0x08, 0x16, 0xf4, 0x4c, 
  0xc3, 0xd6, 0xda, 0x7e, 0x02, 0xe5, 0x9d, 0x3d, 
  0xe7, 0xec, 0x38, 0xd8, 0xae, 0x69, 0xdd, 0x44, 
  0x62, 0x18, 0x7b, 0xba, 0x85, 0xac, 0x17, 0x5c, 
  0x2c, 0xcd, 0xdb, 0x8d, 0x7e, 0x5a, 0xc5, 0x9a, 
  0x8b, 0x8c, 0x89, 0x4e, 0x15, 0xe8, 0x67, 0xa2, 
  0xe2, 0x80, 0x4c, 0x2e, 0x4f, 0x0d, 0xcf, 0xdb, 
  0xb9, 0xbb, 0xb0, 0xd4, 0xac, 0xbd, 0x1e, 0x5f, 
  0xd6, 0xd7, 0x3b, 0x9a, 0xf1, 0xe3, 0xd2, 0x74, 
  0xe6, 0xca, 0xaf, 0x9c, 0x97, 0x4b, 0x33, 0x04, 
  0x40, 0x62, 0x3f, 0x0e, 0xe6, 0xda, 0xf4, 0x65, 
  0x69, 0x06, 0x9e, 0x15, 0x02, 0xfa, 0x6e, 0x7c, 
  0xa6, 0x54, 0xdb, 0x71, 0xad, 0xc8, 0x23, 0x38, 
  0x3a, 0xf3, 0x04, 0x24, 0x0f, 0xaa, 0x10, 0xd0, 
  0xa1, 0xca, 0x25, 0x90, 0x6d, 0xff, 0x72, 0xe6, 
  0x7e, 0x10, 0x42, 0x19, 0x14, 0xf4, 0xc4, 0x20, 
  0x6a, 0xfd, 0x43, 0x5a, 0xa3, 0x7a, 0xc7, 0x8f, 
  0x5b, 0xa1, 0xd0, 0x93, 0xe2, 0xc0, 0xe6, 0x9e, 
  0xc7, 0xbc, 0x82, 0x60, 0x50, 0xc7, 0x8c, 0xd8, 
  0xc1, 0x57, 0x2c, 0x3a, 0xa6, 0x10, 0x4c, 0xdc, 
  0x2b, 0x26, 0x10, 0xe0, 0x35, 0x6f, 0x08, 0xcf, 
  0x09, 0xaf, 0xe8, 0x4a, 0xfa, 0x92, 0x97, 0xf9, 
  0x57, 0x06, 0x3b, 0x24, 0x9a, 0x20, 0x40, 0x82, 
  0xde, 0x46, 0xed, 0xbc, 0x71, 0x33, 0x0c, 0x79, 
  0x52, 0xa4, 0x7f, 0x39, 0x29, 0x99, 0xd9, 0x09, 
  0x55, 0x60, 0x4a, 0xe0, 0x11, 0x3f, 0xee, 0x28, 
  0x64, 0x9f, 0x73, 0x48, 0xcb, 0x4b, 0x26, 0x69, 
  0x46, 0x25, 0x1d, 0x08, 0xd5, 0x49, 0x80, 0x80, 
  0x61, 0xb7, 0x36, 0xf4, 0x83, 0xe4, 0x97, 0x8f, 
  0x9f, 0x56, 0xed, 0x0a, 0x8b, 0x34, 0x4d, 0x7e, 
  0xe7, 0xe2, 0xa9, 0x5b, 0xd0, 0x30, 0x94, 0x01, 
  0x5d, 0xf3, 0x03, 0xa0, 0x61, 0xae, 0x7a, 0xf1, 
  0x22, 0x4b, 0x13, 0xa8, 0xe0, 0x92, 0xca, 0x55, 
  0x5e, 0x02, 0x4d, 0x54, 0xf1, 0xff, 0x08, 0x15, 
  0xbb, 0xb0, 0x07, 0xc5, 0xc4, 0x58, 0x85, 0x33, 
  0x4c, 0xda, 0x4c, 0x2b, 0x58, 0xd3, 0x0a, 0x2e, 
  0xf6, 0xc3, 0x2c, 0x2d, 0x73, 0xe5, 0x64, 0xff, 
  0x2a, 0xf3, 0xa2, 0xf8, 0xac, 0x16, 0x69, 0x23, 
  0x1e, 0x4d, 0x9a, 0xcb, 0x82, 0xad, 0xf4, 0x9a, 
  0xcd, 0xdf, 0x2e, 0x0a, 0xbb, 0x0d, 0xa3, 0x0d, 
  0xd2, 0x1e, 0x45, 0xb9, 0xb0, 0x3b, 0x18, 0xf4, 
  0x68, 0x3b, 0x4b, 0x53, 0x41, 0xd7, 0xac, 0x58, 
  0x9a, 0x3f, 0x2b, 0x52, 0x19, 0x64, 0x9e, 0xc4, 
  0xad, 0xe0, 0x87, 0x7d, 0xc9, 0x33, 0xd6, 0xd2, 
  0xce, 0x1c, 0xc0, 0x9d, 0xd0, 0x50, 0x0a, 0x5a, 
  0xd5, 0x0a, 0x06, 0xc5, 0x43, 0xf8, 0x5b, 0xc0, 
  0xa9, 0x71, 0x87, 0x1f, 0x10, 0xc1, 0x38, 0xb0, 
  0x5c, 0x3f, 0x76, 0xef, 0x7b, 0xfc, 0x59, 0x2a, 
  0xbb, 0x98, 0x6a, 0x79, 0x2a, 0x60, 0xe6, 0xb6, 
  0x24, 0x13, 0xf2, 0xb8, 0x81, 0xc8, 0x93, 0xdb, 
  0x8d, 0xaf, 0x3e, 0x7a, 0x80, 0x06, 0x5d, 0x2d, 
  0x05, 0x7f, 0x62, 0x49, 0xdb, 0x0e, 0xda, 0x61, 
  0x43, 0xed, 0x5e, 0x8b, 0x8a, 0xbc, 0x62, 0xb0, 
  0xf1, 0x04, 0xb6, 0x5d, 0x65, 0x63, 0xe1, 0x1f, 
  0x3c, 0xaf, 0xa6, 0x52, 0x40, 0x9b, 0x89, 0x02, 
  0xb8, 0x29, 0x13, 0xaf, 0x93, 0x65, 0x14, 0xca, 
  0x5f, 0x08, 0x7a, 0x4a, 0x2a, 0x38, 0x09, 0xc7, 
  0x52, 0xbe, 0xd9, 0xd4, 0x4c, 0x26, 0xb8, 0x93, 
  0xf5, 0xfb, 0x9a, 0x50, 0x55, 0x05, 0xe7, 0x39, 
  0x61, 0xd4, 0x0b, 0xdb, 0xca, 0x0b, 0xe2, 0x5e, 
  0xd2, 0xf7, 0x64, 0xd7, 0x82, 0x43, 0x2a, 0x8e, 
  0xfc, 0x5e, 0x03, 0x05, 0xe6, 0xba, 0xfd, 0x48, 
  0xb5, 0x0c, 0x8c, 0x43, 0x0d, 0x5e, 0x2f, 0x14, 
  0xba, 0xaf, 0x69, 0xbf, 0x80, 0xf4, 0xec, 0xb8, 
  0x84, 0xa9, 0xee, 0x90, 0xc9, 0x2d, 0xd6, 0xcf, 
  0x63, 0x96, 0xd7, 0x7b, 0xc8, 0x17, 0x1c, 0x2e, 
  0x0a, 0x8b, 0x47, 0x0e, 0xfd, 0x7d, 0x53, 0xf0, 
  0x63, 0xf2, 0x9c, 0xd7, 0xf9, 0xba, 0x60, 0x8f, 
  0xfa, 0x37, 0x2f, 0x54, 0x40, 0x9d, 0xa8, 0x49, 
  0x45, 0xe6, 0x66, 0x61, 0xba, 0x99, 0xa7, 0x42, 
  0x0f, 0xc5, 0xa1, 0x60, 0x0a, 0xa6, 0xaf, 0xd0, 
  0x73, 0xbb, 0xe4, 0x8c, 0x51, 0xeb, 0x32, 0x53, 
  0x52, 0xf1, 0xc4, 0x44, 0xa3, 0x62, 0x15, 0x85, 
  0xb9, 0xd1, 0x9a, 0xa6, 0x4f, 0x5b, 0x9d, 0x8c, 
  0x84, 0xa6, 0xd0, 0xfd, 0x0e, 0x8a, 0x2e, 0x67, 
  0x40, 0x46, 0x9e, 0xeb, 0xcd, 0x81, 0x74, 0xc2, 
  0x26, 0x7a, 0x72, 0x86, 0xa7, 0x6f, 0xa9, 0x48, 
  0x89, 0x1b, 0x8d, 0xf1, 0x44, 0x81, 0x3f, 0x05, 
  0x94, 0x78, 0x33, 0x40, 0x47, 0x5d, 0xad, 0x81, 
  0x17, 0x8e, 0x05, 0xec, 0xb9, 0xd0, 0xe3, 0x5d, 
  0x32, 0xac, 0x3e, 0xa2, 0x37, 0xd4, 0x45, 0xc1, 
  0xee, 0x10, 0x79, 0x20, 0xf7, 0x43, 0x02, 0xb6, 
  0x93, 0x7e, 0x15, 0x05, 0x51, 0x68, 0xce, 0xf2, 
  0xd1, 0xb1, 0x77, 0x4d, 0xd7, 0xd9, 0x3a, 0xb8, 
  0x34, 0xf1, 0x50, 0x37, 0x28, 0x72, 0xac, 0x08, 
  0x07, 0x4e, 0x14, 0x3c, 0xc4, 0x1e, 0xa4, 0xdb, 
  0x8d, 0xb0, 0x7f, 0xdf, 0xf7, 0xa2, 0x61, 0xb1, 
  0x2b, 0xee, 0xc4, 0x75, 0x75, 0xe1, 0xdd, 0x9b, 
  0x83, 0x61, 0xbb, 0x8b, 0x19, 0x0f, 0xa6, 0x9b, 
  0xba, 0x90, 0x9d, 0x8a, 0x1d, 0x47, 0x93, 0xe8, 
  0xe0, 0x3c, 0x4c, 0x22, 0xe4, 0x23, 0x7f, 0xdc, 
  0x1b, 0xf7, 0x54, 0xee, 0xc6, 0xbd, 0xee, 0xf2, 
  0x6a, 0x9a, 0x51, 0x23, 0x86, 0xf4, 0xec, 0x9a, 
  0x10, 0xc6, 0x7f, 0x5f, 0x29, 0xeb, 0xea, 0xfd, 
  0xeb, 0xa5, 0x3c, 0x70, 0xf8, 0xad, 0x60, 0x0d, 
  0x43, 0x1d, 0x20, 0x06, 0x72, 0x43, 0xcb, 0x7f, 
  0x20, 0x61, 0x64, 0xf9, 0x06, 0x8a, 0x1f, 0x62, 
  0x03, 0xbe, 0x13, 0x2b, 0x05, 0x8a, 0x8a, 0x1f, 
  0xce, 0x98, 0x18, 0x39, 0x68, 0xc4, 0x3f, 0x63, 
  0x7c, 0x5f, 0xaa, 0xa0, 0x87, 0xaa, 0xee, 0x0e, 
  0x07, 0x7c, 0x9a, 0xa6, 0xd3, 0x19, 0xfa, 0x73, 
  0x9e, 0x57, 0x10, 0xb2, 0xe4, 0x02, 0xc1, 0x89, 
  0xff, 0x4c, 0xe5, 0x41, 0x30, 0xc5, 0xce, 0xe1, 
  0x10, 0x58, 0xd8, 0xdb, 0xfe, 0xef, 0x14, 0xf3, 
  0xb7, 0xe6, 0xb8, 0x79, 0x63, 0x47, 0xb3, 0x2c, 
  0x4f, 0x02, 0x42, 0xe3, 0x80, 0x35, 0x28, 0x04, 
  0xfb, 0x00, 0x4a, 0x14, 0x2a, 0x4c, 0x48, 0xfc, 
  0x80, 0xdf, 0x24, 0xda, 0x7f, 0x21, 0xf5, 0x7d, 
  0x31, 0x77, 0x20, 0x7f, 0xc7, 0x6d, 0x15, 0x91, 
  0x2b, 0x8d, 0x35, 0x76, 0xae, 0x34, 0xd6, 0xd0, 
  0x8b, 0x5f, 0x6d, 0xac, 0x6e, 0xfc, 0x4d, 0x1a, 
  0xeb, 0xf7, 0x9c, 0x02, 0x74, 0x76, 0x4d, 0x70, 
  0xbc, 0x2b, 0xe8, 0x7b, 0xa1, 0xf3, 0x3a, 0xfa, 
  0xf8, 0x9b, 0xa3, 0x3f, 0xbd, 0x7a, 0xbc, 0x7a, 
  0x4b, 0x99, 0x5e, 0x64, 0xae, 0x5f, 0x78, 0xce, 
  0x2f, 0x47, 0xc3, 0x1d, 0x2a, 0xf6, 0xcd, 0xd7, 
  0x6f, 0x89, 0x1a, 0xf5, 0x2b, 0x57, 0xc4, 0x4b, 
  0xcd, 0xc5, 0xf9, 0x07, 0xaf, 0x88, 0xff, 0x0f, 
  0x3a, 0x87, 0xef, 0xa0, 0xb3, 0xd7, 0x32, 0xe8, 
  0x75, 0x3a, 0x3b, 0xee, 0xdf, 0xd5, 0x4c, 0xe6, 
  0xaf, 0x1e, 0x97, 0x61, 0xfc, 0xb7, 0xf2, 0xea, 
  0xac, 0x6c, 0x5c, 0xe2, 0xcc, 0x93, 0x70, 0xbd, 
  0x12, 0x7d, 0x6f, 0x9c, 0x84, 0x51, 0x15, 0xbe, 
  0xfb, 0xd5, 0x43, 0x1f, 0x95, 0x0b, 0xf5, 0x2a, 
  0xbc, 0xba, 0xf9, 0x13, 0x7b, 0x7e, 0x0f, 0x9a, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x28, 0x75, 0x75, 0x61, 0x79, 0x29
} };

static GStaticResource static_resource = { ch_resource_data.data, sizeof (ch_resource_data.data), NULL, NULL, NULL };
extern GResource *ch_get_resource (void);
GResource *ch_get_resource (void)
{
  return g_static_resource_get_resource (&static_resource);
}
/*
  If G_HAS_CONSTRUCTORS is true then the compiler support *both* constructors and
  destructors, in a sane way, including e.g. on library unload. If not you're on
  your own.

  Some compilers need #pragma to handle this, which does not work with macros,
  so the way you need to use this is (for constructors):

  #ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
  #pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(my_constructor)
  #endif
  G_DEFINE_CONSTRUCTOR(my_constructor)
  static void my_constructor(void) {
   ...
  }

*/

#ifndef __GTK_DOC_IGNORE__

#if  __GNUC__ > 2 || (__GNUC__ == 2 && __GNUC_MINOR__ >= 7)

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR(_func) static void __attribute__((constructor)) _func (void);
#define G_DEFINE_DESTRUCTOR(_func) static void __attribute__((destructor)) _func (void);

#elif defined (_MSC_VER) && (_MSC_VER >= 1500)
/* Visual studio 2008 and later has _Pragma */

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _wrapper(void) { _func(); return 0; } \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) static int (* _array ## _func)(void) = _func ## _wrapper;

#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _constructor(void) { atexit (_func); return 0; } \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) static int (* _array ## _func)(void) = _func ## _constructor;

#elif defined (_MSC_VER)

#define G_HAS_CONSTRUCTORS 1

/* Pre Visual studio 2008 must use #pragma section */
#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _wrapper(void) { _func(); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (*p)(void) = _func ## _wrapper;

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  section(".CRT$XCU",read)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void); \
  static int _func ## _constructor(void) { atexit (_func); return 0; } \
  __declspec(allocate(".CRT$XCU")) static int (* _array ## _func)(void) = _func ## _constructor;

#elif defined(__SUNPRO_C)

/* This is not tested, but i believe it should work, based on:
 * http://opensource.apple.com/source/OpenSSL098/OpenSSL098-35/src/fips/fips_premain.c
 */

#define G_HAS_CONSTRUCTORS 1

#define G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA 1
#define G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA 1

#define G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(_func) \
  init(_func)
#define G_DEFINE_CONSTRUCTOR(_func) \
  static void _func(void);

#define G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(_func) \
  fini(_func)
#define G_DEFINE_DESTRUCTOR(_func) \
  static void _func(void);

#else

/* constructors not supported for this compiler */

#endif

#endif /* __GTK_DOC_IGNORE__ */

#ifdef G_HAS_CONSTRUCTORS

#ifdef G_DEFINE_CONSTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_CONSTRUCTOR_PRAGMA_ARGS(resource_constructor)
#endif
G_DEFINE_CONSTRUCTOR(resource_constructor)
#ifdef G_DEFINE_DESTRUCTOR_NEEDS_PRAGMA
#pragma G_DEFINE_DESTRUCTOR_PRAGMA_ARGS(resource_destructor)
#endif
G_DEFINE_DESTRUCTOR(resource_destructor)

#else
#warning "Constructor not supported on this compiler, linking in resources will not work"
#endif

static void resource_constructor (void)
{
  g_static_resource_init (&static_resource);
}

static void resource_destructor (void)
{
  g_static_resource_fini (&static_resource);
}
