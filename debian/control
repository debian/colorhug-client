Source: colorhug-client
Maintainer: Debian QA Group <packages@qa.debian.org>
Section: graphics
Priority: optional
Build-Depends: debhelper-compat (= 13),
               bash-completion,
               intltool,
               itstool,
               libtool,
               libxml2-utils,
               docbook-utils,
               yelp-tools,
               docbook,
               gobject-introspection,
               libcolorhug-dev,
               libgusb-dev,
               libcolord-dev,
               libcolord-gtk-dev,
               libgtk-3-dev,
               libcanberra-gtk3-dev,
               libsqlite3-dev,
               libsoup2.4-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/colorhug-client
Vcs-Git: https://salsa.debian.org/debian/colorhug-client.git
Homepage: https://github.com/hughski/colorhug-client/
Rules-Requires-Root: no

Package: colorhug-client
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         librsvg2-common
Suggests: gnome-color-manager,
          argyll
Description: Tools for the Hughski Colorimeter
 The Hughski ColorHug colorimeter is a low cost open-source hardware
 sensor used to calibrate screens.
 .
 This package includes the client tools which allows the user to upgrade
 the firmware on the sensor, set the color calibration matrix or to
 access the sensor.
 .
 Please note that this package does not provide calibration, you
 should install gnome-color-manager or argyll for this purpose.
